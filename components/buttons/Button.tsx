import { NextComponentType } from 'next'

interface ButtonProps {
  color?: 'primary' | 'secondary' | 'sdark' | 'darks'
  size?: 'xl' | 'lg' | 'md' | 'sm'
  rounded?: boolean
  onClick?: any
  children?: any
}

const Button = (props: ButtonProps) => {
  return (
    <>
      <button
        onClick={props.onClick}
        className={`shadow-lg btn-${props.color} btn-${props.size} ${
          props.rounded ? 'rounded-full' : ''
        }`}
      >
        {props.children}
      </button>
    </>
  )
}

export default Button
