import { HTMLInputTypeAttribute } from 'react'
import Input from './input'
import InputLabel from './input-label'

interface InputGroupProps {
  isRequired?: boolean
  htmlFor: string
  type: HTMLInputTypeAttribute
  name: string
  id: string
  placeholder: string
  label: string
}

export default function InputGroup(props: InputGroupProps) {
  return (
    <div className='flex flex-col'>
      <InputLabel htmlFor={props.htmlFor} isRequired={props.isRequired}>
        {props.label}
      </InputLabel>
      <Input
        id={props.id}
        name={props.name}
        placeholder={props.placeholder}
        type={props.type}
      />
    </div>
  )
}
