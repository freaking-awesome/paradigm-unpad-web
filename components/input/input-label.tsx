interface InputLabelProps {
  isRequired?: boolean
  htmlFor: string
  children?: any
}

export default function InputLabel(props: InputLabelProps) {
  return (
    <label htmlFor={props.htmlFor} className='font-bold pb-1'>
      {props.children}{' '}
      {props.isRequired ? <span className='text-red-600'>*</span> : ''}
    </label>
  )
}
