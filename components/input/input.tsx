import { HTMLInputTypeAttribute } from 'react'

interface InputProps {
  type: HTMLInputTypeAttribute
  name: string
  id: string
  placeholder: string
}

export default function Input(props: InputProps) {
  return (
    <input
      type={props.type}
      name={props.name}
      id={props.id}
      placeholder={props.placeholder}
      className='px-4 py-2 rounded-lg bg-dark2 focus:outline-none '
    />
  )
}
