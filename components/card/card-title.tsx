const CardTitle = (props: any) => {
  return (
    <div className='w-auto my-3'>
      <p className='font-bold text-2xl px-5 text-center'>{props.children}</p>
    </div>
  )
}

export default CardTitle
