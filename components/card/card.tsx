const Card = (props: any) => {
  return (
    <>
      <div
        className={`bg-sdark px-9 py-7 mr-8 flex flex-col rounded-lg w-card`}
      >
        {props.children}
      </div>
    </>
  )
}

export default Card
