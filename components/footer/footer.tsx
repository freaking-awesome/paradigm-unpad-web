import Image from 'next/image'
import linkedin from '../../public/linkedin.png'
import twitter from '../../public/twitter.png'
import youtube from '../../public/youtube.png'
import instagram from '../../public/instagram.png'
import ParadigmLogo from '../logo/paradigm-logo'

const Footer = () => {
  return (
    <footer className='flex justify-between flex-col py-14 px-28 bg-sdark w-full'>
      <div>
        <h3 className='text-white font-bold'>
          <ParadigmLogo></ParadigmLogo>
        </h3>
      </div>

      <div className='flex justify-between pt-14 items-center'>
        <div className='flex justify-between text-white'>
          <h3 className='font-bold mr-14'>Events</h3>
          <h3 className='font-bold mr-14'>About Us</h3>
          <h3 className='font-bold mr-14'>Register</h3>
          <h3 className='font-bold'>Partnership</h3>
        </div>

        <div className='flex justify-between items-center'>
          <div className='mr-8'>
            <Image src={twitter} alt='social-icon' className='mr-8' />
          </div>
          <div className='mr-8'>
            <Image src={instagram} alt='social-icon' className='mr-8' />
          </div>
          <div className='mr-8'>
            <Image src={linkedin} alt='social-icon' className='mr-8' />
          </div>
          <div className='mr-8'>
            <Image src={youtube} alt='social-icon' className='mr-8' />
          </div>
        </div>
      </div>

      <div className='pt-14'>
        <div className='text-white'>
          <h3 className='font-light'>&copy; 2022 Paradigm Unpad.</h3>
        </div>
      </div>
    </footer>
  )
}

export default Footer
