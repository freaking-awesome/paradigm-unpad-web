/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      padding: {
        50: '200px',
        btn: '100px',
      },
      screens: {
        desktop: '1441px',
        mobile: '360px',
      },
      fontWeight: {
        semibold: '600',
        bold: '700',
      },
      fontSize: {
        hero: '64px',
      },
      colors: {
        primary: '#FFDE35',
        secondary: '#FFF7CB',
        sdark: '#272833',
        darks: '#1c1c24',
        'secondary-text': '#D3D6E0',
        info: '#EEF2FA',
        sinfo: '#2E5AAC',
        dark3: '#4F5167',
        dark2: '#393A4A',
        light3: '#A1A8BD',
      },
      height: {
        'hero-video': '651px',
        card: '641px',
      },
      width: {
        'hero-video': '1191px',
        card: '486px',
      },
    },
  },
  plugins: [],
}
