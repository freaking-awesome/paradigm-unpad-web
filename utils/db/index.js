import { initializeApp } from 'firebase/app'
import { getDatabase } from 'firebase/database'

const serviceAccount = require('../../paradigm-key.json')

const firebaseConfig = {
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://paradigm-unpad-default-rtdb.asia-southeast1.firebasedatabase.app"
}

const app = initializeApp(firebaseConfig)

const database = getDatabase(app)

export default database
